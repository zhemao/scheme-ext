(define (eratosthenes-iter numbers)
  (if (null? numbers) '()
    (let* ((top (car numbers))
           ;; filter out all the multiples of top
          (filtered 
            (filter 
              (lambda (num) 
                (not (= (modulo num top) 0)))
              (cdr numbers))))
      (cons top (eratosthenes-iter filtered)))))

;; Implementation of the sieve of eratosthenes.
;; Finds all the primes from 2 to maxnum inclusive
(define (eratosthenes maxnum)
  (eratosthenes-iter 
    ;; numbers from 2 to maxnum
    (cddr (iota (+ maxnum 1)))))

(define (factorize-iter num x)
  (cond ((> x num) '())
        ((= (modulo num x) 0) 
         (cons x (factorize-iter (/ num x) x)))
        (else (factorize-iter num (+ x 1)))))

;; returns a list which is the prime factorization of the number
(define (factorize num)
  (factorize-iter num 2))

(define (prime?-iter num x)
  (cond ((> (* x x) num) #\t)
        ((= (modulo num x) 0) #\f)
        (else (prime?-iter num (+ x 1)))))

;; returns whether or not the number is prime
(define (prime? num)
  (prime?-iter num 2))

