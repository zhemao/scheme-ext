;; This file defines some libraries and macros to make looping simpler

;; A Python style list comprehension
;; Makes if possible to do (list-comp (* x 2) x '(1 2 3) (< x 3))
;; Which is converted into
;; (map (lambda (x) (* x 2)) 
;;   (filter (lambda (x) (< x 3)) '(1 2 3)))
;; And thus returns (2 4)

(define-syntax list-comp
  (syntax-rules ()
    ((_ body var lst)
     (map (lambda (var) body) lst))
    ((_ body var lst test)
     (map (lambda (var) body) 
          (filter (lambda (var) test) lst)))))

;; A range function, which returns a list from start to stop
;; Has an optional step parameter
;; Ex. (range 0 5) -> (0 1 2 3 4)
;;     (range 0 5 2) -> (0 2 4)
;;     (range 5 0 -1) -> (5 4 3 2 1)
(define range
  (lambda (start stop . rest)
    (let ((step (if (null? rest) 1 (car rest))))
      (if (or 
            (and (> step 0) (< start stop))
            (and (< step 0) (> start stop)))
        (cons start 
              (range (+ start step) stop step)) 
        '()))))

;; The for macro does a C-style for loop
;; For instance
;; (for x 0 5 1 (* x 2)) -> (0 2 4 6 8)
(define-syntax for
  (syntax-rules ()
    ((_ var start stop step body)
     (map (lambda (var) body)
          (range start stop step)))
    ((_ var start stop body)
     (map (lambda (var) body)
          (range start stop)))))

;; The repeat macro generates a list 
;; made up of the result of body repeated
;; the given number of times
;; For instance
;; (repeat 5 1) -> (1 1 1 1 1)
(define-syntax-rule (repeat times body)
  (map (lambda (_) body)
       (range 0 times)))

